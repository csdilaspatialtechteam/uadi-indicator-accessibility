
/*
 *  Copyright 2016-2017 University of Melbourne
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by: Dr. Yiqun Chen    yiqun.c@unimelb.edu.au
 */

import org.junit.Test;
import uadi.indicator.wps.uwa.Utils;
import uadi.indicator.wps.uwa.indicatorCoreLogic;

import org.geotools.data.simple.SimpleFeatureCollection;
import org.json.JSONArray;
import org.json.JSONObject;

public class IndicatorTest {

	public IndicatorTest() {
		// TODO Auto-generated constructor stub
	}

	//@Test
	public void check() {
		
		System.out.println("===Jenks:"+Utils.isValidColorName("Jenks"));
		System.out.println("=== :"+Utils.isValidColorName(""));
		System.out.println("===Reds:"+Utils.isValidColorName("Reds"));
		System.out.println("===Greens:"+Utils.isValidColorName("Greens"));
		System.out.println("===blues:"+Utils.isValidColorName("blues"));
		System.out.println("===Blues:"+Utils.isValidColorName("Blues"));

		
	}
	/**
	 * Test indicator implementation logic here
	 */
	@Test
	public void execIndicator() {

		try {
			if (!Utils.isGeoServerReachable()) {
				System.out.println("== GeoServer is not reachable.");
				return;
			}

			int thresh = 60;
			//test url for Perth (old zone)
			//String matrix_url = "http://apps.csdila.ie.unimelb.edu.au/tmpdata/PT_Zones_Travel_times_2011.csv";
			//String zoneProfiles_url = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?ontomapid=98&devkey=e2792cb5-0d03-40fd-be66-cd44b238edf9";

			//test url for Sydney
			//String matrix_url = "http://apps.csdila.ie.unimelb.edu.au/tmpdata/tt_matrix_Syd.csv";
			//String zoneProfiles_url = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?ontomapid=99&devkey=e2792cb5-0d03-40fd-be66-cd44b238edf9";

			//test url for Perth (new zone)
			//String matrix_url = "http://apps.csdila.ie.unimelb.edu.au/tmpdata/tt_matrix_Perth.csv";
			//String zoneProfiles_url = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?ontomapid=100&devkey=e2792cb5-0d03-40fd-be66-cd44b238edf9";

			//test url for Brisbane
			//String matrix_url = "http://apps.csdila.ie.unimelb.edu.au/tmpdata/tt_matrix_Brisbane.csv";
			//String zoneProfiles_url = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?ontomapid=101&devkey=e2792cb5-0d03-40fd-be66-cd44b238edf9";

			//test url for Adelaide
			String matrix_url = "http://apps.csdila.ie.unimelb.edu.au/tmpdata/tt_matrix_Adelaide.csv";
			String zoneProfiles_url = "http://apps.csdila.ie.unimelb.edu.au/uadi-service/execengine/concept/getfeatures?ontomapid=102&devkey=e2792cb5-0d03-40fd-be66-cd44b238edf9";

			
			SimpleFeatureCollection zonesProfiles = Utils.getFeatureCollection(zoneProfiles_url);
			System.out.println("=== zonesProfiles.size:  "+ zonesProfiles.size());
			String output_wfsurl = indicatorCoreLogic.UADI_Accessibility_Indicator_CALCULATE(thresh, zonesProfiles,	matrix_url);

			JSONObject output = new JSONObject();
			JSONObject data = new JSONObject();
			
			JSONArray geolayerArr = new JSONArray();
			
			//output
			JSONObject geolayer_msu = new JSONObject();
			geolayer_msu.put("layerdisplayname", "accessibility_index");
			geolayer_msu.put("layername", Utils.getLayerNameFromWFSUrl(output_wfsurl));
			geolayer_msu.put("bbox", Utils.getBbox(zonesProfiles));

			//wfs part
			JSONObject wfs_msu = new JSONObject();
			wfs_msu.put("url", output_wfsurl);
			
			//if necessary, get wfs style parameters created
			JSONObject msuWFSStyleResult = Utils.createWFSStyle(output_wfsurl, "acss_jb_p");
			if(msuWFSStyleResult!=null && msuWFSStyleResult.getInt("status") == 0){
				wfs_msu.put("styleparams", msuWFSStyleResult.getJSONObject("data"));
			}
			
			
			geolayer_msu.put("wfs", wfs_msu);
			
			//wms part, get wms style parameters created
			JSONObject msuWMSStyleResult = Utils.createWMSStyle(output_wfsurl, "acss_jb_p", "Reds", false, "polygon", 6, "Jenks");
			if(msuWMSStyleResult!=null && msuWMSStyleResult.getInt("status") == 0){
				JSONObject wms_msu = new JSONObject();
				wms_msu.put("url", Utils.getWMSUrlTemplate());
				wms_msu.put("styleparams", msuWMSStyleResult.getJSONObject("data"));
				geolayer_msu.put("wms", wms_msu);
			}

			geolayerArr.put(geolayer_msu);
			
			data.put("geolayers", geolayerArr);
			output.put("data", data);
			output.put("status", 0);
			
			System.out.println("== output: " + output.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("== execIndicator test done");
		return;
	}

}
