package uadi.indicator.wps.uwa;

import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.referencing.FactoryException;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;

import uadi.indicator.wps.uwa.Utils;
import uadi.indicator.wps.uwa.ZoneInfo;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.json.JSONObject;

public class indicatorCoreLogic {

	public static String UADI_Accessibility_Indicator_CALCULATE(int threshold, SimpleFeatureCollection zoneProfiles,
			String matrixUrl) {
		// this line enforce the geometry coordinates are in lon-lat order when
		// result is published in GeoServer
		System.setProperty("org.geotools.referencing.forceXY", "true");

		double[][] ttMatrix = null;

		// ============================================================
		// getting the content of travel time matrix
		// ============================================================
		try {

			InputStream in = null;
			StringWriter writer = null;
			HttpURLConnection connection = null;

			URL url = new URL(matrixUrl);

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);

			in = (InputStream) connection.getInputStream();
			writer = new StringWriter();
			IOUtils.copy(in, writer);
			String matrixContent = writer.toString();
			String[] csv_lines = matrixContent.split(System.getProperty("line.separator"));
			ttMatrix = new double[csv_lines.length][];

			int lineNumber = 0;
			for (String s_csv1 : csv_lines) {
				String[] tmp_comma_splitted = s_csv1.split(",");
				int columnNumber = 0;
				ttMatrix[lineNumber] = new double[tmp_comma_splitted.length];

				for (String s_csv1_D : tmp_comma_splitted) {
					try {
						ttMatrix[lineNumber][columnNumber] = Double.parseDouble(s_csv1_D);
					} catch (Exception e) {
						//if travel time is not available (e.g., marked as '-' in the matrix csv file), set it as a big number
						ttMatrix[lineNumber][columnNumber] = Double.MAX_VALUE;
					}

					columnNumber++;
				}
				lineNumber++;
			}

			if (writer != null) {
				writer.close();
			}
			if (in != null) {
				in.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// do nothing
		}

		// CREATING THE OUTPUTS
		// ============================================================
		// ============================================================

		SimpleFeatureIterator zonesIter = zoneProfiles.features();

		HashMap<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("zones", zonesIter);
		inputs.put("matrix", ttMatrix);

		HashMap<String, Integer> zoneJobInfo = new HashMap<String, Integer>();
		HashMap<String, Integer> zoneLabourForceInfo = new HashMap<String, Integer>();

		SimpleFeatureIterator zonesIter2 = zoneProfiles.features();

		while (zonesIter2.hasNext()) {

			SimpleFeatureImpl f = (SimpleFeatureImpl) zonesIter2.next();
			String location_id = f.getAttribute("id").toString();
			JSONObject location_profile = new JSONObject(f.getAttribute("has_location_profile").toString());
			int total_emplymentForZone = Integer.parseInt(
					((JSONObject) location_profile.get("Location_Profile")).get("total_employment").toString());
			int number_of_labour_forceForZone = Integer.parseInt(
					((JSONObject) location_profile.get("Location_Profile")).get("number_of_labour_force").toString());
			// System.out.println(total_emplymentForZone);

			zoneJobInfo.put(location_id, total_emplymentForZone);
			zoneLabourForceInfo.put(location_id, number_of_labour_forceForZone);
		}

		String output = createOutputForThreshold(threshold + " minutes", zoneProfiles, inputs, zoneJobInfo,
				zoneLabourForceInfo, threshold);

		// ============================================================
		// ============================================================
		return output;

	}

	private static String createOutputForThreshold(String title, SimpleFeatureCollection sfc,
			HashMap<String, Object> inputs, HashMap<String, Integer> zoneJobInfo,
			HashMap<String, Integer> zoneLabourForceInfo, int threshold) {

		SimpleFeatureIterator zonesIter = (SimpleFeatureIterator) inputs.get("zones");
		double[][] travelTimesMatrix = (double[][]) inputs.get("matrix");

		int total_number_of_jobs = 0;
		int total_number_of_labourForce = 0;

		ArrayList<ZoneInfo> zonesInf = new ArrayList<ZoneInfo>();

		while (zonesIter.hasNext()) {

			SimpleFeatureImpl f = (SimpleFeatureImpl) zonesIter.next();
			Geometry location_geom = (Geometry) f.getDefaultGeometry();
			String location_id = f.getAttribute("id").toString();
			JSONObject location_profile = new JSONObject(f.getAttribute("has_location_profile").toString());
			int total_emplymentForZone = Integer.parseInt(
					((JSONObject) location_profile.get("Location_Profile")).get("total_employment").toString());
			int number_of_labour_forceForZone = Integer.parseInt(
					((JSONObject) location_profile.get("Location_Profile")).get("number_of_labour_force").toString());


			total_number_of_jobs += total_emplymentForZone;
			total_number_of_labourForce += number_of_labour_forceForZone;

			// calculate step 1
			// 1. Determine which zones can be reached within the selected
			// travel time threshold by the selected travel mode,
			// e.g. within 30 minutes by car. [The zone is the trip origin (home
			// end].)

			// ALSO TOGETHER WITH

			// Calculate step 2
			// 2. Add up the relevant land use, e.g. jobs, in all of the zones
			// that can be reached and allocate this number to the zone

			// find the zone "row" in the matrix
			int RowIndex = -1;
			// double[] TravelTimesFromZoneToOtherZones = null;
			double[] zonesNames = travelTimesMatrix[0];

			for (int cnt = 1; cnt <= travelTimesMatrix[0].length - 1; cnt++) {
				Double v = travelTimesMatrix[cnt][0];
				// System.out.println("[" + location_id + "][" +
				// ((Integer)v.intValue()).toString() + "]");

				if (location_id.equalsIgnoreCase(((Integer) v.intValue()).toString())) {
					RowIndex = cnt;
					// TravelTimesFromZoneToOtherZones = travelTimesMatrix[cnt];
					break;
				}
			}

			int totalJobsAccessibleFromThisZone = 0;
			int totalLabourForceAccessibleFromThisZone = 0;

			ArrayList<String> ZonesWithinTheTravelTimeThresholdForJobs = new ArrayList<String>();
			ArrayList<String> ZonesWithinTheTravelTimeThresholdForLabourforce = new ArrayList<String>();

			for (int cntot = 1; cntot < travelTimesMatrix.length - 1; cntot++) {

				if (travelTimesMatrix[RowIndex][cntot] < threshold) {

					Double tmp_zoneName = zonesNames[cntot];
					ZonesWithinTheTravelTimeThresholdForJobs.add(((Integer) tmp_zoneName.intValue()).toString());
					totalJobsAccessibleFromThisZone += zoneJobInfo.get(((Integer) tmp_zoneName.intValue()).toString());
				}

				if (travelTimesMatrix[cntot][RowIndex] < threshold) {
					// System.out.println("[" + cntot + "] [" + RowIndex + "] =
					// " + travelTimesMatrix[cntot][RowIndex] + " < " +
					// threshold);
					Double tmp_zoneName = zonesNames[cntot];
					ZonesWithinTheTravelTimeThresholdForLabourforce.add(((Integer) tmp_zoneName.intValue()).toString());
					totalLabourForceAccessibleFromThisZone += zoneLabourForceInfo
							.get(((Integer) tmp_zoneName.intValue()).toString());
					;
				}

			}

			ZoneInfo zi = new ZoneInfo(location_id, location_geom, totalJobsAccessibleFromThisZone,
					totalLabourForceAccessibleFromThisZone, total_emplymentForZone, number_of_labour_forceForZone);
			zonesInf.add(zi);

		}
		zonesIter.close();

		//////////////////////////////////////
		// the output geometry attribute name has to be set as "the_geom",
		// otherwise GeoTools lib will not create a shpfile from the constructed
		////////////////////////////////////// output SimpleFeatureCollection
		String outGeomName = "the_geom";
		String zoneid = "zone_id";
		String zn_no_jb = "zn_no_jb";
		String zn_no_wf = "zn_no_wf";
		String acss_jb = "acss_jb";
		String acss_wf = "acss_wf";
		String acss_wf_p = "acss_wf_p";
		String acss_jb_p = "acss_jb_p";

		// build output feature type
		SimpleFeatureTypeBuilder stbMsuOutput = new SimpleFeatureTypeBuilder();
		stbMsuOutput.setName("MSUOutput");
		stbMsuOutput.setCRS(sfc.getSchema().getCoordinateReferenceSystem());
		stbMsuOutput.add(outGeomName, MultiPolygon.class);
		stbMsuOutput.setDefaultGeometry(outGeomName);

		// copy attributes from msu population datalayer
		for (AttributeDescriptor attDisc : sfc.getSchema().getAttributeDescriptors()) {
			String name = attDisc.getLocalName();
			Class type = attDisc.getType().getBinding();
			if (attDisc instanceof GeometryDescriptor) {
				// skip existing geometry field since we have created a new
				// one 'the_geom'
			} else {
				// try to keep all rest fields, since the field name cannot
				// extend 10 chars, trim it if too long.

				stbMsuOutput.add(Utils.clapFieldName(name), type);
			}
		}
		// add new attributes for holding the size (area) of green area and
		// index value

		stbMsuOutput.add(zoneid, Integer.class);
		stbMsuOutput.add(zn_no_jb, Integer.class);
		stbMsuOutput.add(zn_no_wf, Double.class);
		stbMsuOutput.add(acss_jb, Integer.class);
		stbMsuOutput.add(acss_wf, Integer.class);
		stbMsuOutput.add(acss_jb_p, Double.class);
		stbMsuOutput.add(acss_wf_p, Double.class);

		// prepare output FeatureCollection
		SimpleFeatureType sftMsuOutput = stbMsuOutput.buildFeatureType();
		SimpleFeatureBuilder sfbMsuOutput = new SimpleFeatureBuilder(sftMsuOutput);
		DefaultFeatureCollection fcMSUOutputPrj = new DefaultFeatureCollection();

		SimpleFeatureIterator itrMsu = sfc.features();

		while (itrMsu.hasNext()) {

			SimpleFeatureImpl f = (SimpleFeatureImpl) itrMsu.next();

			int zonecode = Integer.parseInt(f.getAttribute("id").toString());

			SimpleFeature newf = sfbMsuOutput.buildFeature(null);

			for (AttributeDescriptor attDisc : sfc.getSchema().getAttributeDescriptors()) {
				String name = attDisc.getLocalName();
				// if current field is a geometry, we save it to the
				// appointed field 'the_geom'
				if (attDisc instanceof GeometryDescriptor) {
					newf.setAttribute("the_geom", f.getDefaultGeometry());
				} else // otherwise, copy field value respectively
				{
					newf.setAttribute(Utils.clapFieldName(name), f.getAttribute(name));
				}
			}

			for (ZoneInfo zi : zonesInf) {
				if (Integer.parseInt(zi.getName()) == zonecode) {

					zi.setJobPercentage(1.0 * zi.getTotalJobs() / total_number_of_jobs);
					zi.setLabourForcePercentage(1.0 * zi.getTotalLabourForce() / total_number_of_labourForce);
					newf.setAttribute(zoneid, zonecode);
					newf.setAttribute(zn_no_jb, zi.getNoOfJobs());
					newf.setAttribute(zn_no_wf, zi.getNoOfLabourForce());
					newf.setAttribute(acss_jb, zi.getTotalJobs());
					newf.setAttribute(acss_wf, zi.getTotalLabourForce());
					newf.setAttribute(acss_jb_p, zi.getJobPercentage());
					newf.setAttribute(acss_wf_p, zi.getLabourForcePercentage());

					break;
				}

			}

			// add newly built feature to FeatureCollection
			fcMSUOutputPrj.add(newf);

		}
		itrMsu.close();

		String output_mus_wfsurl = "";
		try {
			output_mus_wfsurl = Utils.publishFeatureCollection(fcMSUOutputPrj, "accs_idx");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output_mus_wfsurl;
	}

}
