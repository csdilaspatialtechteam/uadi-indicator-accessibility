/*
 *  Copyright 2016-2017 University of Melbourne
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by: Dr. Yiqun Chen    yiqun.c@unimelb.edu.au
 */
package uadi.indicator.wps.uwa;

import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.process.factory.DescribeParameter;
import org.geotools.process.factory.DescribeProcess;
import org.geotools.process.factory.DescribeResult;
import org.geotools.process.factory.StaticMethodsProcessFactory;
import org.geotools.text.Text;
import org.json.*;

public class IndicatorAccessibility extends StaticMethodsProcessFactory<IndicatorAccessibility> {

	public IndicatorAccessibility() {
		//set title and namespace for a process, the namespace here has to be set as "custom"
		super(Text.text("UADI Indicator Accessibility"), "custom", IndicatorAccessibility.class);
	}

	@DescribeProcess(title = "execIndicatorAccessibility", description = "calculates the accessible jobs and labour force for each zone and according to the travel time threshold indicated (in minutes).")
	@DescribeResult(description = "outputs are wrapped in wfs urls")
	public static String execIndicatorAccessibility(
			@DescribeParameter(name = "zoneprofile_wfsurl", description = "wfsurl for zone profile (polygon expected)") String zoneprofiles_wfsurl,
			@DescribeParameter(name = "traveltime_matrix", description = "URL of the travel time matrix (values in minutes)") String traveltime_matrix,
			@DescribeParameter(name = "threshold", description = "threshold (in minutes)") int thresholdValue,
			@DescribeParameter(name = "colorname", description = "color name for output map (e.g., Reds, Blues, Greens. http://colorbrewer2.org)") String colorname,
			@DescribeParameter(name = "colornum", description = "color number for output map (e.g., 1-9)") int colornum,
			@DescribeParameter(name = "colorclassifier", description = "color classifier name (e.g., Jenks, EqualInterval, Quantile, StandardDeviation)") String classifier,
			@DescribeParameter(name = "jobuuid", description = "job uuid, if provided, the output will be updated in that job") String jobuuid) {
		JSONObject output = new JSONObject();
		
		try {
			
			//########################################
			// Check if GeoServer is reachable
			//########################################
			// this step is important if indicator outputs need to be temporarily stored on a geoserver (recommended)
			// this step can be skipped if there is no requirement of using geoserver to store outputs (NOT recommended)
			if (!Utils.isGeoServerReachable()) {
				output.put("errdesc", "GeoServer is not reachable");
				output.put("status", 1);
				return output.toString();
			}

			if(!Utils.isValidColorNum(colornum)) colornum = 6;
			if(!Utils.isValidClassifier(classifier)) classifier = "Jenks";
			if(!Utils.isValidColorName(colorname)) colorname = "Reds";
			
			SimpleFeatureCollection zonesProfiles = Utils.getFeatureCollection(zoneprofiles_wfsurl);
			
			// RUN THE INDICATOR LOGIC and get the published wfs url in the geoserver
			String output_wfsurl = indicatorCoreLogic.UADI_Accessibility_Indicator_CALCULATE(thresholdValue, zonesProfiles, traveltime_matrix);
			
			//########################################
			// Wrap up indicator outputs
			//########################################
			// prepare for the output json MUST be organised in following structure:
			// { "status": 0,
			//	 "data" :{
			//		"geolayers":[
			//						{
			//							"layername":"WORKSPACE_NAME:DATALAYER_NAME",
			//							"layerdisplayname":"A_READABLE_NAME_FOR_YOUR_INDICATOR_OUTPUT_DATALAYER",
			//							"bbox":[],
			//							"wfs": {
			//								"url":"",
			//								//ATTENTION: wfs style is NOT used in UADI currently, it is reserved for future development
			//								"styleparams":{ // if provided, this can be used for automatic client-side style creation
			//										"attrname":"", //attribute name that the style is created for
			//										"minval":min,
			//										"maxval":max,
			//										"meanval":mean,
			//										"sdval": standard deviation
			//										}
			//									},
			//							"wms": {
			//								"url":"http://xxxx/geoserver/WORKSPACE_NAME/wms",
			//								//ATTENTION: wms style is mandatory and MUST be created so that the layer can be visualised in UADI.
			//								"styleparams":{
			//										"attrname":"", //attribute name that the style is created for
			//										"stylename":"",
			//										"sldurl":"",
			//										"legendurl":""
			//										}
			//									}
			//						}
			//					],
			//		"tables":[//this structure is complex enough to describe a matrix data like csv
			//					{
			//						"title": "Your Table Title",
			//						"data":[
			//					              {
			//					            	  "colname":"column1",
			//					            	  "values":[0,1,2,3,4,5]
			//					              },
			//					              {
			//					            	  "colname":"column2",
			//				            	  	  "values":["Azadeh","Sam","Benny","Soheil","Mohsen","Abbas"]
			//					              }
			//								]
			//					}		
			//		         ],
			//		"charts":[ // this contains data structure for 8 types of charts supported by UADI including:
			//				   // columnchart, barchart, linechart, areachart, radialchart, piechart, donutchart, scatterchart 
			//		              {
			//						  "type" : "columnchart", // "barchart", "linechart", "areachart" and "radialchart" shares the exactly same data structure of "columnchart"
			//		            	  "seqnum":1, // the visualisation sequence of charts 
			//		            	  "title": "Your Chart Title", // the title of chart
			//						  "stacked": false, // whether to put yfileds on stack, this ONLY applies to "columnchart" and "barchart" when multiple yfields provided
			//						  "xfield": "month", // a categorical attribute 
			//						  "yfield": ["data1", "data2", "data3", "data4"], // an array of numeric y field(s), even with one element
			//						  "yfieldtitle": [ "IE", "Firefox", "Chrome", "Safari"], // an array of title of y field(s) used in legend, even with one element  
			//						  "data": [// prepared the data in json array format, xfield should be in the first column, the rest should be yfields 
			//					                { "month": "Jan", "data1": 20, "data2": 37, "data3": 35, "data4": 4 },
			//					                { "month": "Feb", "data1": 20, "data2": 37, "data3": 36, "data4": 5 },
			//					                { "month": "Mar", "data1": 19, "data2": 36, "data3": 37, "data4": 4 },
			//					                { "month": "Apr", "data1": 18, "data2": 36, "data3": 38, "data4": 5 },
			//					                { "month": "May", "data1": 18, "data2": 35, "data3": 39, "data4": 4 },
			//					                { "month": "Jun", "data1": 17, "data2": 34, "data3": 42, "data4": 4 },
			//					                { "month": "Jul", "data1": 16, "data2": 34, "data3": 43, "data4": 4 },
			//					                { "month": "Aug", "data1": 16, "data2": 33, "data3": 44, "data4": 4 },
			//					                { "month": "Sep", "data1": 16, "data2": 32, "data3": 44, "data4": 4 },
			//					                { "month": "Oct", "data1": 16, "data2": 32, "data3": 45, "data4": 4 },
			//					                { "month": "Nov", "data1": 15, "data2": 31, "data3": 46, "data4": 4 },
			//					                { "month": "Dec", "data1": 15, "data2": 31, "data3": 47, "data4": 4 }
			//					            ]
			//		              },
			//		              {
			//						  "type" : "piechart",  //"donutchart" shares the exactly same data structure of "piechart"
			//		            	  "seqnum":2, // the visualisation sequence of charts 
			//		            	  "title": "Your Chart Title", // the title of chart
			//						  "donut": false, // set false to create a piechart, set true to create a donutchart
			//						  "xfield": "month", // a categorical attribute 
			//						  "yfield": "data1", // a numeric attribute 
			//						  "data": [// prepared the data in json array format, xfield should be in the first column, the rest should be yfield 
			//									{ "month": "Jan", "data1": 68 },
            //									{ "month": "Feb", "data1": 17 },
            //									{ "month": "Mar", "data1": 15 }
			//					            ]
			//		              },
			//		              {
			//						  "type" : "scatterchart",
			//		            	  "seqnum":3, // the visualisation sequence of charts 
			//		            	  "title": "Your Chart Title", // the title of chart
			//						  "xfield": "x", // a numeric attribute 
			//						  "yfield": "y", // a numeric attribute 
			//						  "data": [// prepared the data in json array format 
			//									{ "x": 5, "y": 20 },
            //									{ "x": 480, "y": 90 },
            //									{ "x": 250, "y": 50 },
            //									{ "x": 100, "y": 33 },
            //									{ "x": 330, "y": 95 },
            //									{ "x": 410, "y": 12 },
            //									{ "x": 475, "y": 44 }
			//					            ]
			//		              }
			//		            ]
			//
			//
			//	},
			//   "errdesc": ""
			// }
			// "status" 0 means no error occurrs during the processing, 1 means processing has errors. Integer 0 or 1 (rather than string "0" or "1") is expected. 
			// "data" attribte can be either JSONObject (recommended) or JSONArray, it is the place where the outputs should be stored. 
			// "errdesc" contains the error descriptions, it should be empty when "status" sets to 0
			JSONObject data = new JSONObject();
			
			JSONArray geolayerArr = new JSONArray();
			
			//output
			JSONObject geolayer_msu = new JSONObject();
			geolayer_msu.put("layerdisplayname", "accessibility_index");
			geolayer_msu.put("layername", Utils.getLayerNameFromWFSUrl(output_wfsurl));
			geolayer_msu.put("bbox", Utils.getBbox(zonesProfiles));

			//wfs part
			JSONObject wfs_msu = new JSONObject();
			wfs_msu.put("url", output_wfsurl);
			
			//if necessary, get wfs style parameters created
			JSONObject msuWFSStyleResult = Utils.createWFSStyle(output_wfsurl, "acss_jb_p");
			if(msuWFSStyleResult!=null && msuWFSStyleResult.getInt("status") == 0){
				wfs_msu.put("styleparams", msuWFSStyleResult.getJSONObject("data"));
			}
			
			geolayer_msu.put("wfs", wfs_msu);
			
			//wms part, get wms style parameters created
			JSONObject msuWMSStyleResult = Utils.createWMSStyle(output_wfsurl, "acss_jb_p", colorname, false, "polygon", colornum, classifier);
			if(msuWMSStyleResult!=null && msuWMSStyleResult.getInt("status") == 0){
				JSONObject wms_msu = new JSONObject();
				wms_msu.put("url", Utils.getWMSUrlTemplate());
				wms_msu.put("styleparams", msuWMSStyleResult.getJSONObject("data"));
				geolayer_msu.put("wms", wms_msu);
			}

			geolayerArr.put(geolayer_msu);
			
			data.put("geolayers", geolayerArr);
			output.put("data", data);
			output.put("status", 0);

		} catch (Exception e) {
			output.put("errdesc", e.getMessage());
			output.put("status", 1);
		}

		//########################################
		// Sync indicator outputs to an indicator job
		//########################################
		// update job outputs if jobuuid is provided
		if (jobuuid != null && jobuuid !="") {
			System.out.println("=== output:"+output.toString());
			Utils.updateJob(output, jobuuid);
		}
		return output.toString();
	}

}