package uadi.indicator.wps.uwa;

import com.vividsolutions.jts.geom.Geometry;

public class ZoneInfo {
	
	private String zoneName = "";
	private Geometry geom = null;
	private int total_jobsOfThisZone = 0;
	private int total_labourForceOfThisZone = 0;
	private int total_jobsAccessibleForThisZone = 0;
	private int total_labourForceAccessibleForThisZone = 0;
	private double jobPercentage = 0;
	private double labourForcePercentage = 0;
	private int ranking = 0;
	
	
	public ZoneInfo()
	{
		// empty constructor
	}
			
	public ZoneInfo(String name, Geometry geometry, int totalJobs, int totalLabourForce, int zoneJobs, int zoneLabourForce)
	{
		this.zoneName = name;
		this.geom = geometry;
		this.total_jobsAccessibleForThisZone = totalJobs;
		this.total_labourForceAccessibleForThisZone = totalLabourForce;
		this.total_jobsOfThisZone = zoneJobs;
		this.total_labourForceOfThisZone = zoneLabourForce;
	}
	
	public int getNoOfJobs() {
		 return this.total_jobsOfThisZone; 
	}
	
	public void setNoOfJobs(int jobs)
	{
		this.total_jobsOfThisZone = jobs;
	}
	
	public int getNoOfLabourForce() {
		 return this.total_labourForceOfThisZone; 
	}
	
	public void setNoOfLabourForce(int labourForce)
	{
		this.total_labourForceOfThisZone = labourForce;
	}
	
	
	public int getRanking() {
		 return this.ranking; 
	}
	
	public void setRanking(int rank)
	{
		this.ranking = rank;
	}
	
	public double getLabourForcePercentage() {
		 return this.labourForcePercentage; 
	}
	
	public void setLabourForcePercentage(double labourForcePercent)
	{
		this.labourForcePercentage = labourForcePercent;
	}
	
	
	public double getJobPercentage() {
		 return this.jobPercentage; 
	}
	
	public void setJobPercentage(double jobPercent)
	{
		this.jobPercentage = jobPercent;
	}
	
	
	
	public int getTotalLabourForce() {
		 return this.total_labourForceAccessibleForThisZone; 
	}
	
	public void setTotalLabourForce(int totalLabourForce)
	{
		this.total_labourForceAccessibleForThisZone = totalLabourForce;
	}
	
	
	
	public int getTotalJobs() {
		 return this.total_jobsAccessibleForThisZone; 
	}
	
	public void setTotalJobs(int totalJobs)
	{
		this.total_labourForceAccessibleForThisZone = totalJobs;
	}
	
	
	
	public String getName() {
		 return this.zoneName; 
	}
	
	public void setName(String name)
	{
		this.zoneName = name;
	}
	
	public Geometry getGeometry() {
		 return this.geom; 
	}
	
	public void setGeometry(Geometry geometry) {
		 this.geom = geometry; 
	}
	

}
